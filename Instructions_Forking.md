# Forking Instructions
These instructions are for a project hosted on Gitlab.


1. Ensure you have a Gitlab account.
2. Once logged in a fork button will appear on the main page of the repo, click this button.   
   ![forking.png](media/forking_1.png)
3. Work on your own copy of the repo that was created.
4. When done on teh web interface select the "Merge Requests" menu.
   * Source is your own repo and branch.
   * Target is this repo (``c2842/semester-4/cs4076_robocode`` for class of 2023) 
5. "Compare branches and continue"
6. There is a bit on this page but the key part is "Create merge request".
7. The maintainer of teh core repo will be able to accept teh merge request in.