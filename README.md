# CS4076 Robocode

Welcome to teh repo for CS4076 Robocode


## Useful info
* [Forking and merging instructions](./Instructions_Forking.md)
* [How to setup the repo with Intellij (recommended)](./Instructions_Setup.md)
* Several sample bots can be found in teh ``src`` folder (``ch.zuehlke`` ``Misc``)
  * The sentry bot can be found in ``UL``
  * Past 1st year duel bots can be found in ``UL`` as well.

## Past Competitions
In each past competition folder there should be all the bots for said competition as well as the Rules for that competition.  
Some will also include a ``.battle`` which is the config file for the battle.

* [2023](./src/CS4076/_2023)