package TUS._2022;
import robocode.*;
import static robocode.util.Utils.normalRelativeAngleDegrees;
import robocode.HitByBulletEvent;
import robocode.Robot;
import robocode.WinEvent;
import java.awt.*;
//import java.awt.Color;


 // MurkyWater - a robot by (LEE + ISHKA)//
 
public class TUS_MurkyWater extends Robot
{
boolean begin = true;	
	//Setting robots colour palette//
	public void run() {
		setBodyColor(Color.black);
		setGunColor(Color.red);
		setRadarColor(Color.red);
	
		setAdjustRadarForRobotTurn(true);//turns radar left and right to scan robot//
	 while(true) {
	 if (begin){
		ahead(30);//beginning position//
		turnRadarRight(180);
		begin=false;
		}
		turnRadarRight(180);
		}
	}


	public void onScannedRobot(ScannedRobotEvent e) {
		//If a enemy robot is scanned we start firing bullets but it differs depending on distance//
		double absoluteBearing = getHeading() + e.getBearing();
    	double bearingFromGun = normalRelativeAngleDegrees(absoluteBearing - getGunHeading());
     if (e.isSentryRobot()==false){    //dont shoot sentry bot//
	turnGunRight(bearingFromGun);
	double range = e.getDistance(); //if the distance is greater than 400 fire 2//
	if (range > 400) {
		fire(2);
		}
	else if(e.getDistance() < 50 & getEnergy() > 50) { //else fire 3//
	  fire(3);
	}	

	fire(3);
	}
}	

	//what to do when we get hit by a bullet//
	public void onHitByBullet(HitByBulletEvent e) {
		// move back 30//
		back(30);
	}
	
	
	public void onHitWall(HitWallEvent e) {
		//if we get hit by a wall we move either left or right to avoid sentry//
		double loc;
		loc=e.getBearing();
		if(loc>0){
			turnLeft(80);
		}// if we are in a right corner turn left a move away//
		else{
		turnRight(80);
		}//else we turn right and move ahead//
		ahead(40);
	}
	public void onWin(WinEvent e) {  //A victory dance if we win//
		for (int i = 0; i < 50; i++) {
			turnRight(40);
			turnLeft(30);
		}
	}
}

