package MU._2022;
import robocode.*;
import robocode.util.Utils;

import java.awt.Color;

// API help : https://robocode.sourceforge.io/docs/robocode/robocode/Robot.html

/**
 * aak - a robot by Anhelina Kukharenka.
 */
public class MU_aak extends Robot
{
	double lastScanDist; //stores distance so its available for getPowe
	
	public void run() {
		setAdjustGunForRobotTurn(false); //set so gun turns independent of body
		setAdjustRadarForGunTurn(false); //set so radar turns independent of gun
		setAdjustRadarForRobotTurn(false); //set so radar turns independent of body
		
		setColors(Color.pink,Color.white,Color.orange); // body,gun,radar
		
		
		while(true) {
			
			turnRadarRight(45); // default behaviour, robot turns radar 45 degrees
		}
	}
	
	public void onScannedRobot(ScannedRobotEvent e) { 
		if(!e.isSentryRobot()) //if enemy robot is scanned by radar:
		{ 
			lastScanDist =e.getDistance(); //storing distance 
			turnGunRight(Utils.normalRelativeAngleDegrees((e.getBearing()+getHeading()-getGunHeading()))); //gun turns right by (angle normalized to give range 
				//-180 to 180( gives angle gun needs to point (getHeading()-getGunHeading() gets gun to be on top of heading. the gun is then turned e.getBearing at enemy)
			fire(getPowe()); //calls getPowe method to determine the power of the bullet to fire, then fires
			turnRight(Utils.normalRelativeAngleDegrees(e.getBearing()+70)); //get the angle(location of the enemy relative to where my heading is),
				//add 70(dodging enemy bullets this way), so I am closing in on enemy.
			
			ahead(80); //goes ahead 80 pixels
		}
	}
	public double getPowe() //this method determines the power of the bullet to be fired
	{
		double d = lastScanDist; //distance to the enemy robot scanned
		if (d<160)
		{
			return 3-0.5*(d/50); //a higher power bullet is fired the closer the robot is to the enemy. lowest it gets is 1.5. 
		}
		else
		{
			return 1.5; //lower power fired due to bigger distance
		}
	}
	
	public void onHitRobot(HitRobotEvent e) { //if robot body hits enemy  body, this method will be called
		if(e.getBearing()<0) //finds the angle the enemy is at relative to my robots heading, does the following then based on whether the enemy is left or right of mine:
		{
			turnRight(90); //turns right by 90 degrees
			ahead(80); //ahead 80 pixels
		}
		else
		{
			turnRight(-90); //turns left 90 degrees
			ahead(80); //ahead 80 pixels
		}
	
	}
	
	public void onHitWall(HitWallEvent e) { //this method works the same as the onHitRobot method, except this method gets called when my robot hits a wall.
		
		if(e.getBearing()<0)
		{
			turnRight(90);
			ahead(80);
		}
		else 
		{
			turnRight(-90);
			ahead(80);
		}
	}	
}
