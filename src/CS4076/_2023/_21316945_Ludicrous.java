package CS4076._2023;

import robocode.AdvancedRobot;
import robocode.HitByBulletEvent;
import robocode.HitRobotEvent;
import robocode.ScannedRobotEvent;

import java.awt.*;


public class _21316945_Ludicrous extends AdvancedRobot {
   public void run() {
     // Set colors
     setBodyColor(Color.orange);
     setGunColor(Color.blue);
     setRadarColor(Color.orange);
     setScanColor(Color.black);

     while (true) {
         if(getOthers()<14 && getEnergy()>20){
             setMaxTurnRate(4);
             turnRight(360);
         }
         else{
             setTurnRight(5000);
             setMaxVelocity(5);

             ahead(10000);
         }
       }
   }

   /**
    * onScannedRobot: Fire hard!
    */
   public void onScannedRobot(ScannedRobotEvent e) {
     if (e.isSentryRobot()) {
         turnRight(90);
       return;
     }
     if((getOthers()<10 || e.getEnergy()<40)&&(getEnergy()>20)){
         setTurnRight(e.getBearing());
         setFire(3);
         // And move forward
         setAhead(100);
         setTurnGunRight(100);
         setTurnGunLeft(200);

         execute();
     }
     if(getEnergy()<15){
           fire(0.1);
           turnLeft(170);
           ahead(50);
       }
     else if(e.getDistance()<200) {
           fire(3);

     }
     else{
         fire(1);

     }
   }

   public void onHitRobot(HitRobotEvent e) {
     if (e.getBearing() > -90 && e.getBearing() < 90) {
       fire(3);
     }
     if (e.isMyFault()) {
         turnRight(15);
     }
   }

   public void onHitByBullet(HitByBulletEvent e) {
     turnRight(10 - e.getBearing());
     ahead(15);
     turnGunLeft(10);
   }
}