package CS4076._2023;
import robocode.*;
import robocode.AdvancedRobot;
import robocode.ScannedRobotEvent;
//import java.awt.Color;

// API help : https://robocode.sourceforge.io/docs/robocode/robocode/Robot.html

/**
 * CS4076_21307571_KEVINHOUGH - a robot by (your name here)
 */
public class _21307571_KEVINHOUGH extends AdvancedRobot
{
	double enemyDistance, enemyHeading, enemyBearing;

	public void run() {
		// Initialization of the robot should be put here

		// After trying out your robot, try uncommenting the import at the top,
		// and the next line:

		// setColors(Color.red,Color.blue,Color.green); // body,gun,radar

		// Robot main loop
		while(true) {
			// Replace the next 4 lines with any behavior you would like
			setTurnRight(10000);
			setMaxVelocity(3);
			ahead(10000);
		}
	}

	/**
	 * onScannedRobot: What to do when you see another robot
	 */
	public void onScannedRobot(ScannedRobotEvent e) {
		fire(3);
	}

	public void onHitbyBullet(HitByBulletEvent e){

		if (Math.random() < 1){
			turnLeft(10000);
		} else{
			turnRight(10000);
		}
	}

	/**
	 * onHitWall: What to do when you hit a wall
	 */
	public void onHitWall(HitWallEvent e) {
		// Replace the next line with any behavior you would like
		setTurnLeft(10000);
	}
}
