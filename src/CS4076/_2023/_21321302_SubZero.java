package CS4076._2023;

import java.awt.*;
import robocode.AdvancedRobot;
import robocode.HitRobotEvent;
import robocode.ScannedRobotEvent;
import robocode.TurnCompleteCondition;

public class _21321302_SubZero extends AdvancedRobot {
  public void run() {
    setBodyColor(Color.blue);
    setGunColor(Color.blue);
    setRadarColor(Color.black);
    setScanColor(Color.yellow);
    setBulletColor(Color.black);

	int i = 0;
    while (true) {
      Distance a = distFromWall(getX(), getY());
      if (a.distance <= 70) {
        turnOppositeFromWall(a);
        setAhead(50);
        waitFor(new TurnCompleteCondition(this));
      }
	  i++;

	  if(i % 5 == 0) {
		turnOppositeFromWall(a);
		setAhead(100);
		execute();
		waitFor(new TurnCompleteCondition(this));
	  }
      setTurnRight(7500);
      setMaxVelocity(5);
      setAhead(10000);
      waitFor(new TurnCompleteCondition(this));
    }
  }

  public void onScannedRobot(ScannedRobotEvent e) {
	if(e.isSentryRobot()) return;
    int fireAmount = 2;
	if(e.getEnergy() < 15) {
		fireAmount++;
	}
	if (e.getDistance() < 100) {
	  fireAmount++;
	}
	if (e.getDistance() < 50) {
	  fireAmount++;
	}
	if (e.getDistance() < 10) {
	  fireAmount++;
	}
	fire(fireAmount);
  }

  public void onHitRobot(HitRobotEvent e) {
    if (e.getBearing() > -10 && e.getBearing() < 10) {
      fire(2);
    }
    if (e.isMyFault()) {
      Distance a = distFromWall(getX(), getY());
      turnOppositeFromWall(a);
      setMaxVelocity(5);
      setAhead(100);
      waitFor(new TurnCompleteCondition(this));
    }
  }

  public boolean isNearWall(double threshold) {
    double xPos = getX();
    double yPos = getY();
    double battlefieldWidth = getBattleFieldWidth();
    double battlefieldHeight = getBattleFieldHeight();

    Distance d = distFromWall(xPos, yPos);
    return d.distance <= threshold;
  }

  public Distance distFromWall(double x, double y) {
    double battlefieldWidth = getBattleFieldWidth();
    double battlefieldHeight = getBattleFieldHeight();

    double distanceToTopWall = battlefieldHeight - y;
    double distanceToBottomWall = y;
    double distanceToLeftWall = x;
    double distanceToRightWall = battlefieldWidth - x;

    // get the smallest distance, add it to a new Distance objcet, alongside
    // whether it is top, bottom, left or right
    double smallestAmount =
        Math.min(Math.min(distanceToTopWall, distanceToBottomWall),
            Math.min(distanceToLeftWall, distanceToRightWall));
    String name = smallestAmount == distanceToTopWall ? "top"
        : smallestAmount == distanceToBottomWall      ? "bottom"
        : smallestAmount == distanceToLeftWall        ? "left"
                                                      : "right";
    return new Distance(smallestAmount, name);
  }

  public void turnOppositeFromWall(Distance a) {
    double heading = getHeading();
    if (a.side == "top") {
      setTurnRight(90);
    } else if (a.side == "bottom") {
      setTurnRight(270);
    } else if (a.side == "left") {
      setTurnRight(180);
    } else if (a.side == "right") {
      setTurnRight(heading);
    }
  }
}

class Distance {
  public double distance;
  public String side;

  public Distance(double distance, String side) {
    this.distance = distance;
    this.side = side;
  }
}