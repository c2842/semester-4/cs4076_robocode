package CS4076._2023;
import robocode.*;
import robocode.Robot;


import java.awt.*;
public class _21334269_robo extends AdvancedRobot {
    int gunDirection = 1;

    public void run() {
        
        setBodyColor(Color.blue);
        setRadarColor(Color.green);
        setGunColor(Color.black);
        setBulletColor(Color.orange);
        
        while (true) {
            turnGunRight(360);
        }
    }

    public void onScannedRobot(ScannedRobotEvent e) {
        setTurnRight(e.getBearing());
        setFire(3);
        setAhead(100);
        gunDirection = -gunDirection;
        setTurnGunRight(360 * gunDirection);
        execute();
    }
}