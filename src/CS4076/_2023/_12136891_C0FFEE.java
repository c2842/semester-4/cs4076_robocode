package CS4076._2023;

import robocode.Event;
import robocode.*;
import robocode.util.Utils;

import java.awt.*;
import java.util.ArrayList;

import static java.lang.Math.PI;


class Point {
  public double x;
  public double y;

  public Point (double x, double y){
    this.x = x;
    this.y = y;
  }
}

public class _12136891_C0FFEE extends AdvancedRobot {
  public void run() {
    setup();
    move();
  }

  private void setup() {
    // set colours
    setBodyColor(Color.lightGray);
    setRadarColor(Color.yellow);
    // all will bow down to teh might of COFFEE!!!!!!
    setBulletColor(Color.decode("#C0FFEE"));
    setScanColor(Color.GREEN);
    setAdjustRadarForRobotTurn(true);
    setAdjustGunForRobotTurn(true);
    setAdjustRadarForGunTurn(true);
  }

  // movement stuff here
  private void move() {
    int border = getSentryBorderSize();

    // stay this far in from teh edge
    double offset = 20;
    double h = getBattleFieldHeight();
    double w = getBattleFieldWidth();

    double x = getX();
    double y = getY();

    // these are the corners
    int stops = 3;
    int divisor = stops + 1;
    ArrayList<Point> corners = new ArrayList<>();

    // left
    corners.add(new Point(border + offset, border + offset));
    for (int i = 1; i < divisor; i++) {
      corners.add(new Point(border + offset, (i * h) / divisor));
    }

    // top
    corners.add(new Point(border + offset, h - (border + offset)));
    for (int i = 1; i < divisor; i++) {
      corners.add(new Point((i * w) / divisor, h - (border + offset)));
    }

    // left
    corners.add(new Point(w - (border + offset), h - (border + offset)));
    for (int i = 1; i < divisor; i++) {
      corners.add(new Point(w - (border + offset), ((divisor - i) * h) / divisor));
    }

    // bottom
    corners.add(new Point(w - (border + offset), border + offset));
    for (int i = 1; i < divisor; i++) {
      corners.add(new Point(((divisor - i) * w) / divisor, border + offset));
    }


    int moves = 0;
    // find the closest point
    double distance = Double.MAX_VALUE;
    for (int i = 0; i < corners.size(); i++) {
      Point cords = corners.get(i);
      double x_tmp = cords.x;
      double y_tmp = cords.y;
      double distance_tmp = Math.sqrt((Math.pow(x_tmp - x, 2)) + (Math.pow(y_tmp - y, 2)));
      if (distance_tmp < distance) {
        distance = distance_tmp;
        moves = i;
      }
    }

    while (getEnergy() > 0) {
      // spin at teh start of every movement
      spinRadar();

      Point cords = corners.get(moves % corners.size());
      goTo(cords.x, cords.y);
      moves += 1;
      execute();
    }
  }

  // yoinked from https://robowiki.net/wiki/GoTo and modified for Robot
  private void goTo(double x, double y) {
    double x_dest = x - getX();
    double y_dest = y - getY();

    double goAngle = 0;

    double distance = Math.hypot(x_dest, y_dest);
    if (x_dest >= 0 && y_dest >= 0) {
      goAngle = Math.asin(x_dest / distance);
    }
    if (x_dest >= 0 && y_dest < 0) {
      goAngle = PI - Math.asin(x_dest / distance);
    }
    if (x_dest < 0 && y_dest < 0) {
      goAngle = PI + Math.asin(-x_dest / distance);
    }
    if (x_dest < 0 && y_dest >= 0) {
      goAngle = 2.0 * PI - Math.asin(-x_dest / distance);
    }

    // convert to degrees
    goAngle *= 57.2958;

    // compensate for teh current heading
    goAngle -= getHeading();

    // reduce turns if it's possible
    if (goAngle < 0) {
      goAngle += 360;
    }

    // easier to go teh other way if its turning right 3 times to go left
    if (goAngle > 270) {
      goAngle -= 270;
      turnLeft(goAngle);
    } else {
      turnRight(goAngle);
    }

    ahead(distance);
  }

  boolean scan = true;

  private void spinRadar() {
    // do whatever ye want with teh radar, max spin is 360 though
    int max = 0;
    int degrees = 30;
    scan = true;
    while (scan) {
      turnRadarRight(degrees);
      max += degrees;

      if (max > 360) {
        break;
      }
    }
  }

  private String target;
  private double target_distance;

  // this is whenever teh scanner picks up something
  @Override
  public void onScannedRobot(ScannedRobotEvent event) {
    // no need to deal with these
    if (event.isSentryRobot()) {
      return;
    }

    // reduce calls as much as possible
    String name = event.getName();
    double distance = event.getDistance();

    // pick first target if current one is dead
    if (target == null) {
      target = name;
      target_distance = distance;
      this.out.println("Tracking " + name);
    }
    // however if there is a closer target then go for that
    if (distance < target_distance) {
      target = name;
      target_distance = distance;
      this.out.println("Tracking " + name);
    }

    // break any existing scan
    scan = false;


    if (target.equals(event.getName())) {
      fire_sub(event);
    }
  }

  public void onHitRobot(HitRobotEvent event) {
    fire_sub(event);

    //back(30);
  }

  @Override
  public void onRobotDeath(RobotDeathEvent event) {
    if (event.getName().equals(target)) {
      target = null;
      target_distance = Integer.MAX_VALUE;
    }
  }

  private void fire_sub(Event event) {
    if (!(event instanceof ScannedRobotEvent || event instanceof HitRobotEvent)) {
      return;
    }

    double distance = 0;
    double bearing;
    if (event instanceof ScannedRobotEvent) {
      distance = ((ScannedRobotEvent) event).getDistance();
      bearing = ((ScannedRobotEvent) event).getBearing();
    } else {
      bearing = ((HitRobotEvent) event).getBearing();
    }

    double heading = getHeading();
    double heading_gun = getGunHeading();

    // taken inspiration from teh sample tracker firing
    double absoluteBearing = heading + bearing;
    double bearingFromGun = Utils.normalRelativeAngleDegrees(absoluteBearing - heading_gun);

    // turn gun to target
    turnGunRight(bearingFromGun);

    if (getGunHeat() == 0.0D) {
      if (distance > 300) {
        // if the bullets energy is above 1 it does bonus damage, so basically free damage
        fire(1.1);
      } else if (distance > 200) {
        fire(2.0);
      } else {
        // just in case the rules change use the enum
        fire(Rules.MAX_BULLET_POWER);
      }
    }

  }

}