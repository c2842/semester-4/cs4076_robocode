package CS4076._2023;

import robocode.*;
import robocode.util.Utils;

import java.awt.*;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Vector;

/*
    * UBot - by Quentin
    * Credits to:
    * - Coriantrum for the risk movement
    * - Robocode wiki for the Guess factor
    * - Robocode wiki for the wave surfing
 */

/*
    For gun:
        The bot use guess factor targeting, it is saving position of a player and try to determine what will be the next
        position of the player. We can then shoot according to position and keep track of if we hit or no.
    For movement:
        The bot use two type of movement depending on the situation, if we are in melee, we will use the risk movement.
        If we are in 1V1 we will use the wave surfing movement. The risk movement is a movement that will try to avoid
        bullets by moving in a way where the risk is less. The wave surfing movement is a movement that will generate a wave
        at each bullet fired and will try to surf the wave to avoid the bullet.
        (No more wave surfing, it was to overkill)
 */

public class _22378723_UBot extends AdvancedRobot {
    static Movement movement;
    static GuessFactor gun;

    public void run() {
        setAdjustRadarForGunTurn(true);
        setAdjustRadarForRobotTurn(true);
        setAdjustGunForRobotTurn(true);

        setTurnRadarRight(Double.POSITIVE_INFINITY);
        setAllColors(Color.GREEN);

        if (movement == null)
            movement = new Movement(this);
        movement.init();

        if(gun == null)
            gun = new GuessFactor(this);
        gun.init();

        do {
            //Movement
            movement.run();
            execute();
        }
        while (true);
    }

    public void onRoundEnded(RoundEndedEvent e) {
        //Movement
        movement.onRoundEnded(e);
        gun.cleanUp();
    }

    public void onDeath(DeathEvent event) {
        if(getOthers() > 0)
            gun.cleanUp();
    }

    public void onScannedRobot(ScannedRobotEvent e) {
        //MOVEMENT
        movement.onScannedRobot(e);
        //GUN
        gun.onScannedRobot(e);
    }

    public void onHitByBullet(HitByBulletEvent e) {
        //Movement
        movement.onHitByBullet(e);
    }

    public void onRobotDeath(RobotDeathEvent e) {
        //Movement
        movement.onRobotDeath(e);
    }

}

class _22378723_UBot_Helper {
    //utility functions, I figure anyone making a goto bot has something like these, and they
    //come in useful in a lot of other random places, too:
    public static Point2D projectPoint(Point2D startPoint, double theta, double dist) {
        return new Point2D.Double(startPoint.getX() + dist * Math.sin(theta), startPoint.getY() + dist * Math.cos(theta));
    }

    //This strange habit of putting point2 before point1 in this method - and the funny thing is I think
    //I've seen other people do the same thing:
    public static double angle(Point2D point2, Point2D point1) {
        return Math.atan2(point2.getX() - point1.getX(), point2.getY() - point1.getY());
    }

    //returns true if a point is within a truncated field
    public static boolean inField(Point2D p, double width, double height) {
        return new Rectangle2D.Double(30, 30, width - 60, height - 60).contains(p);
    }

    public static double absoluteBearing(Point2D.Double source, Point2D.Double target) {
        return Math.atan2(target.x - source.x, target.y - source.y);
    }

    public static double limit(double min, double value, double max) {
        return Math.max(min, Math.min(value, max));
    }

    public static double bulletVelocity(double power) {
        return (20.0 - (3.0 * power));
    }

    public static double maxEscapeAngle(double velocity) {
        return Math.asin(8.0 / velocity);
    }
}

class Movement {
    AdvancedRobot bot;

    static Point2D myLocation, last;
    Point2D next;
    EnemyRobot currentTarget;
    HashMap<String, EnemyRobot> enemies;

    public Movement(AdvancedRobot bot) {
        this.bot = bot;
    }

    //NOT MINE
    private double findRisk(Point2D point) {
        double risk = 0;
        Collection<EnemyRobot> enemySet = enemies.values();
        Iterator<EnemyRobot> it = enemySet.iterator();
        do {
            EnemyRobot e = it.next();
            //start with an anti-gravity-type value:
            double thisRisk = (e.getEnergy() + 50) / point.distanceSq(e);
            //Here, I'm counting the enemies that are closer to e than the proposed point is.
            //The more, the better, since this robot would be less likely to target me.
            int closer = 0;
            Iterator<EnemyRobot> it2 = enemySet.iterator();
            do {
                EnemyRobot e2 = it2.next();
                if (e.distance(e2) * .9 > e.distance(point))
                    closer++;
            }
            while (it2.hasNext());
            //try this, multiply this number by the number of enemies to which I would be the closest:
            //if they are likely targeting me, multiply by some factor that has to do with "perpendicularity"
            //(I think I remember David Alves calling it that):
            if (closer <= 1 || e.getLastHit() > bot.getTime() - 200 || e == currentTarget) {
                //I had to think about this one, but I think DuelistMicroMelee helped me make sure that it was correct.  I can't remember anymore.
                thisRisk *= 2 + 2 * Math.abs(Math.cos(_22378723_UBot_Helper.angle(myLocation, point) - _22378723_UBot_Helper.angle(e, myLocation)));
            }
            risk += thisRisk;
        }
        while (it.hasNext());
        //add a small antigravity value to the point I was at before now, just to kind of push me in straight lines
        risk += Math.random() / last.distanceSq(point);
        //and repel my current location, to make it more likely that I change my mind before I get to my assigned
        //point (and less likely to change my mind right away maybe):
        risk += Math.random() / 5 / myLocation.distanceSq(point);
        return risk;
    }

    public void init()
    {
        enemies = new HashMap<>();
        currentTarget = null;
    }

    public void run()
    {
        myLocation = new Point2D.Double(bot.getX(), bot.getY());
        if (currentTarget != null) {
            if (next == null)
                next = last = myLocation;
            if (next.distance(myLocation) < 15 || bot.getOthers() > 1) {
                boolean changed = false;
                double angle = 0;
                double moveDist = myLocation.distance(currentTarget);
                moveDist = Math.min(500, moveDist) * .5;
                //if I'm close to my target, I'll move a random direction, otherwise I'll move towards my target:
                do {
                    Point2D p = _22378723_UBot_Helper.projectPoint(myLocation, angle, moveDist);
                    //if I'm not going to hit a wall, and it's a better point than the one I'm going to, then I'll go there:
                    if (_22378723_UBot_Helper.inField(p, bot.getBattleFieldWidth(), bot.getBattleFieldHeight()) && findRisk(p) < findRisk(next)) {
                        changed = true;
                        next = p;
                    }
                    angle += .1;
                }
                while (angle < Math.PI * 2);
                if (changed)
                    last = myLocation;
            }
            double a1, a2;
            //this is the part that makes me turn towards my target point:
            bot.setTurnRightRadians(a1 = Math.atan(Math.tan(a2 = Utils.normalRelativeAngle(_22378723_UBot_Helper.angle(next, myLocation) - bot.getHeadingRadians()))));
            bot.setAhead(Math.abs(bot.getTurnRemainingRadians()) > 1 ? 0 : (a1 == a2 ? 1.0 : -1.0) * next.distance(myLocation));
        }
    }

    public void onHitByBullet(HitByBulletEvent e) {
        EnemyRobot enemy;
        if ((enemy = enemies.get(e.getName())) != null)
            enemy.setLastHit(bot.getTime());
    }

    public void onScannedRobot(ScannedRobotEvent e) {
        //TEST AND ADD NEW ENEMY IF NECESSARY
        String name = e.getName();
        EnemyRobot enemy = enemies.get(name);
        if (enemy == null)
            enemies.put(name, enemy = new EnemyRobot());

        //UPDATE ENEMY INFORMATION
        double distance = e.getDistance(), absBearing = bot.getHeadingRadians() + e.getBearingRadians();
        Point2D myLocation = new Point2D.Double(bot.getX(), bot.getY());

        enemy.setEnergy(e.getEnergy());

        //UPDATE TARGET INFORMATION
        if (currentTarget == null || distance < myLocation.distance(currentTarget))
            currentTarget = enemy;
        double velocity = e.getVelocity();
        enemy.setVelocity(velocity);
        enemy.setLocation(_22378723_UBot_Helper.projectPoint(myLocation, absBearing, distance));
    }

    public void onRoundEnded(RoundEndedEvent event) {
        enemies.clear();
    }

    public void onRobotDeath(RobotDeathEvent e) {
        if (enemies.remove(e.getName()) == currentTarget)
            currentTarget = null;
    }
}

class GuessFactor {
    AdvancedRobot bot;
    static EnemyRobot currentTarget;
    HashMap<String, EnemyRobot> enemies;
    static HashMap<String, int[][][][]> stats = new HashMap<>();

    public GuessFactor(AdvancedRobot bot) {
        this.bot = bot;
    }

    public void init() {
        enemies = new HashMap<>();
        stats = new HashMap<>();
    }

    public void cleanUp()
    {
        stats.clear();
        enemies.clear();
        currentTarget = null;
    }

    public void onScannedRobot(ScannedRobotEvent e) {
        if(e.isSentryRobot())
            return;
        String name = e.getName();
        EnemyRobot enemy = enemies.get(name);
        if (enemy == null)
            enemies.put(name, enemy = new EnemyRobot());
        //Segmentation based on lateral direction and acceleration
        int[][][][] currentStats = stats.computeIfAbsent(name, k -> new int[2][3][13][31]);

        double distance = e.getDistance();
        double absBearing = bot.getHeadingRadians() + e.getBearingRadians();
        Point2D myLocation = new Point2D.Double(bot.getX(), bot.getY());
        Point2D loc = _22378723_UBot_Helper.projectPoint(myLocation, absBearing, distance);
        enemy.setLocation(loc);

        //If we don't have a target, or this enemy is closer than our current target, set this enemy as our target
        if (currentTarget == null || distance < myLocation.distance(currentTarget))
            currentTarget = enemy;
        double velocity = e.getVelocity();
        enemy.setVelocity(velocity);
        //Calculate acceleration
        int acceleration = (int) Math.round(Math.abs(enemy.getVelocity()) - Math.abs(velocity));
        if (acceleration != 0)
            acceleration = (acceleration < 0) ? 1 : 2;

        //Calculate lateral direction
        double lateralDirection = e.getHeadingRadians() - absBearing;
        double energy = e.getEnergy();
        double power = Math.min(Math.min(3, 1200 / distance), Math.min(bot.getEnergy(), energy));
        double bulletVelocity = 20 - 3 * (power / 4);
        //This is the array that stores the number of times we've seen a bullet fired at a particular guess factor
        //The first index is the number of enemies, the second is the acceleration, the third is the distance, and the fourth is the guess factor
        int[] current = currentStats[Math.min(bot.getOthers() - 1, 1)]
                [bot.getOthers() == 1 ? acceleration : (int) (Math.cos(lateralDirection) * velocity / Math.abs(velocity) * 1.4 + 1.4)]
                [(int) (distance / bulletVelocity / 15)];

        //This is the direction the bullet is traveling, relative to the direction the enemy is moving
        double direction = (((Math.sin(lateralDirection) * velocity) < 0) ? -1 : 1) * Math.asin(8 / bulletVelocity);

        //Create waves and update them
        Vector<WaveBullet> waves = enemy.getWaves();
        int i = waves.size();
        while (i > 0) {
            i--;
            if ((waves.elementAt(i)).updateEnemy(loc, bot.getTime()))
                waves.removeElementAt(i);
        }
        //Add a new wave to the list of waves
        WaveBullet wave = new WaveBullet(myLocation, loc, absBearing, direction, bulletVelocity, bot.getTime(), current);
        waves.add(wave);

        //This is the guess factor that we're going to use to fire at the enemy
        if (enemy == currentTarget) {
            int bestIndex = 15;
            double shotBearing = absBearing;
            if (energy > 0)
                do {
                    //Get best index for guess factor and avoid shooting walls
                    double tempBearing = absBearing + direction * (i / 15.0 - 1);
                    if (_22378723_UBot_Helper.inField(_22378723_UBot_Helper.projectPoint(
                                    myLocation, tempBearing, e.getDistance() * (bulletVelocity / (bulletVelocity + 8))),
                            bot.getBattleFieldWidth(), bot.getBattleFieldHeight())
                            && current[i] > current[bestIndex]) {
                        bestIndex = i;
                        shotBearing = tempBearing;
                    }
                    i++;
                }
                while (i < 31);

            //Turn gun to the best guess factor
            bot.setTurnGunRightRadians(Utils.normalRelativeAngle(shotBearing - bot.getGunHeadingRadians()));
            //Fire if the gun is close enough to the best guess factor
            if (Math.max(Math.abs(bot.getGunTurnRemaining()), energy / bot.getEnergy()) < 5)
                bot.setFire(power);
        }
    }
}

class EnemyRobot extends Point2D.Double{
    long lastHit;
    double energy, velocity;
    Vector<WaveBullet> waves = new Vector<>();

    public void update(double energy, double velocity) {
        this.energy = energy;
        this.velocity = velocity;
    }

    public double getVelocity() {
        return velocity;
    }

    public void setVelocity(double velocity) {
        this.velocity = velocity;
    }

    public Vector<WaveBullet> getWaves() {
        return waves;
    }

    public double getEnergy() {
        return energy;
    }

    public long getLastHit() {
        return lastHit;
    }

    public void setLastHit(long lastHit) {
        this.lastHit = lastHit;
    }

    public void setEnergy(double energy) {
        this.energy = energy;
    }
}

class WaveBullet {
    Point2D startPoint, lastPoint;
    double startGunHeading, direction, bulletSpeed, bulletDistance = 0;
    long lastTime;
    int[] segments;

    public WaveBullet(Point2D startPoint, Point2D lastPoint, double startGunHeading, double direction, double bulletSpeed, long lastTime, int[] segments) {
        this.startPoint = startPoint;
        this.lastPoint = lastPoint;
        this.startGunHeading = startGunHeading;
        this.direction = direction;
        this.bulletSpeed = bulletSpeed;
        this.lastTime = lastTime;
        this.segments = segments;
    }

    //returns true if this wave hit and should be removed
    public boolean updateEnemy(Point2D enemy, long time) {
        //I just linearly interpolate where they've been if we don't get an update each tick.
        long dtime;
        double dx = (enemy.getX() - lastPoint.getX()) / (dtime = time - lastTime);
        double dy = (enemy.getY() - lastPoint.getY()) / dtime;
        while (lastTime < time) {
            if (startPoint.distance(lastPoint) <= bulletDistance)    //could have hit now.
            {
                //increments the appropriate bucket
                segments[Math.min(30, Math.max(0, (int) Math.round((1 + Utils.normalRelativeAngle(_22378723_UBot_Helper.angle(lastPoint, startPoint) - startGunHeading) / direction) * 15)))]++;
                return true;
            }
            lastTime++;
            bulletDistance += bulletSpeed;
            lastPoint.setLocation(lastPoint.getX() + dx, lastPoint.getY() + dy);
        }
        return false;
    }
}

