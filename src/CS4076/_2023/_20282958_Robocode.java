package CS4076._2023;

import robocode.*;

public class _20282958_Robocode extends Robot {

    public void run() {
        while (true) {
            ahead(100);
            turnGunRight(360);
            back(100);
            turnGunRight(360);
        }
    }

    public void onScannedRobot(ScannedRobotEvent e) {
        fire(1);
    }

    public void onHitByBullet(HitByBulletEvent e) {
        if (Math.random() < 0.5) {
            turnLeft(90);
        } else {
            turnRight(90);
        }
        ahead(100);
    }
}
