package CS4076._2023;



import robocode.AdvancedRobot;
import robocode.HitRobotEvent;
import robocode.HitWallEvent;
import robocode.ScannedRobotEvent;

import java.awt.*;



public class _21332797_MYroboDivine extends AdvancedRobot {




    public void run() {
        // Set colors
        setBodyColor(Color.blue);
        setGunColor(Color.blue);
        setRadarColor(Color.black);
        setScanColor(Color.yellow);

        // Loop forever
        while (true) {
            // Tell the game that when we take move,
            // we'll also want to turn right... a lot.
            setTurnRight(1000);
            // Limit our speed to 5
            setMaxVelocity(6);
            // Start moving (and turning)
            ahead(1000);
            // Repeat.
        }
    }


    public void onScannedRobot(ScannedRobotEvent e) {
        fire(3);

    }


    public void onHitRobot(HitRobotEvent e) {
        if (e.getBearing() > -10 && e.getBearing() < 10) {
            fire(5);
        }
        if (e.isMyFault()) {
            turnRight(10);
        }
    }

    public void onHitWall(HitWallEvent e){

        back(3);
    }
}
