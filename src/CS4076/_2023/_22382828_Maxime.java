/*
	Note: was submitted after teh competition occured
*/

package CS4076._2023;

import robocode.*;
import java.awt.Color;

public class _22382828_Maxime extends AdvancedRobot {

	int lr = 1;
	
	public void run() {
		while (true) {
			turnGunRight(360);
			execute();
		}
	}

	public void onScannedRobot(ScannedRobotEvent e) {
		setTurnRight(e.getBearing());
		lr *= -1;
		setFire(3);
		if(e.getDistance() < 50)
			setBack(20);
		else
			setAhead(30);
		setTurnGunRight(360 * lr);
  }
}