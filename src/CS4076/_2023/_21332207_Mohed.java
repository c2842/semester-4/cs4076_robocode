/**
 * Copied from MyFirstRobot
 */
/**
 * Copied from MyFirstRobot
 */

// Important to have
package CS4076._2023;

import robocode.*;

import java.awt.*;

import static robocode.util.Utils.normalRelativeAngleDegrees;

public class _21332207_Mohed extends AdvancedRobot {
  boolean movingForward;

  public void run() {
    // Set colors
    setBodyColor(new Color(0, 0, 200));
    setGunColor(new Color(0, 0, 100));
    setRadarColor(new Color(0, 0, 200));
    setBulletColor(new Color(0, 0, 100));
    setScanColor(new Color(0, 0, 100));


    // Loop forever
    while (true) {
      // Tell the game we will want to move ahead 50000
      setAhead(50000);
      movingForward = true;
      //tank moving forward
      setTurnRight(90);
      waitFor(new TurnCompleteCondition(this));
      //method makes the tank wait before it carries out its next action
      //TurnCompleteCondition is a conditon. Tank must wait till turn before performing action.
      setTurnLeft(180);
      //wait for the turn to finish
      waitFor(new TurnCompleteCondition(this));
      //then the other way
      setTurnRight(180);
      //and wait for that turn to finish.
      waitFor(new TurnCompleteCondition(this));
      //then back to the top to do it all again

    }
  }

  public void onHitWall(HitWallEvent e) {
    reverseDirection();
    // the tank reverses when it hits a wall
  }

  public void reverseDirection() {
    if (movingForward) {
      setBack(40000);
      movingForward = false;
    } else {
      setAhead(40000);
      movingForward = true;
    }
    //method to reverse the tanks movements
  }

  public void onScannedRobot(ScannedRobotEvent e) {
    if (e.isSentryRobot()) {
      return;
      // avoids sentry robot
    }
    double Bearing = getHeading() + e.getBearing();
    double bearingFromGun = normalRelativeAngleDegrees(Bearing - getGunHeading());
    // gets the location of the robot

    if (Math.abs(bearingFromGun) <= 3) {
      turnGunRight(bearingFromGun);
      // If it's close in range the tank shoots!

      if (getGunHeat() == 0) {
        fire(Math.min(3 - (bearingFromGun), getEnergy() - .1));
      }//The bearingFromGun variable represents the angle between the gun's current
      // direction and the direction of the enemy robot,
      // We check gun heat here, to see if its ready to fire
      //The getEnergy() - .1 calculates the remaining energy of the robot
      // avoid overheating.
      //method calcutates the minimum energy required to shoot
    }
    else {
      turnGunRight(bearingFromGun);
    }// otherwise just set the gun to turn.


    if (bearingFromGun == 0) {
      scan();//if no tanks scan again
    }
  }


  public void onHitRobot(HitRobotEvent e) {
    if (e.isMyFault()) {
      reverseDirection();
    }    // tank reverses after hitting another tank

    if (e.getEnergy() > 16) {
      fire(3);
    } else if (e.getEnergy() > 10) {
      fire(2);
    } else if (e.getEnergy() > 4) {
      fire(1);
    }    // while tank reverses it shoots the tank it rammed with a shot of a specific power

  }
}