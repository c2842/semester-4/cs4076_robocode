package CS4076._2023;
import robocode.*;

import java.awt.*;


// API help : https://robocode.sourceforge.io/docs/robocode/robocode/Robot.html

/**
 * Spirit_of_Fire - a robot by Valentin
 */
public class _22378642_Spirit_of_Fire extends AdvancedRobot
{

	/**
	 * run: Spirit_of_Fire's default behavior
	 */
	
	//use when you found a target to shoot
	String target = null;
	
	//Use to enter the "panic" mod and be harder to be hit
	boolean panic = false;
	
	//distance from the target
	double distance = 0;
	
	//energy of the robot
	double energy;
	
	//calculate time for searching target
	int count = 0;
	
	//calculate number of damage
	int damage = 0;
	
	//How much should we move our gun
	int direction = 10;
	
	//size of the arena
	int size = 975;
	

	public void run() {
		setBodyColor(new Color(137, 30, 30));
		setGunColor(new Color(225, 222, 215));
		setRadarColor(new Color(255, 204, 153));		
		setBulletColor(new Color(255, 128, 0));
		setScanColor(new Color(255, 0, 0));
		// Initialization of the robot should be put here

		// After trying out your robot, try uncommenting the import at the top,
		// and the next line:

		// setColors(Color.red,Color.blue,Color.green); // body,gun,radar
		// Robot main loop
		while (true)
		{
			out.println(target);
			if(target != null)
			{
				setBodyColor(new Color(243, 114, 87));
				setGunColor(new Color(243, 218, 213));
				setRadarColor(new Color(255, 195, 0));		
				setBulletColor(new Color(199, 0, 57));
				setScanColor(new Color(255, 0, 0));
				
				turnGunRight(direction);
				count ++;
				if(count > 11)
				{
					target = null;
					turnGunRight(getHeading() - getGunHeading());
				}
					
				else if (count > 5)
				{
					direction = 10;
				}
				else if (count > 2)
				{
					direction = -10;
				}
			}
			
			else
			{	
				setBodyColor(new Color(137, 30, 30));
				setGunColor(new Color(225, 222, 215));
				setRadarColor(new Color(255, 204, 153));		
				setBulletColor(new Color(255, 128, 0));
				setScanColor(new Color(255, 0, 0));
				
				setAhead(500);
				setTurnRight(100);
				if(getX() > size || getX() < 225 || getY() < 225 || getY() > size)
				{
					stop();
					turnRight(75);
					ahead(300);
				}
				else
				{
					waitFor(new TurnCompleteCondition(this));
				}
				
			}
		}
	}
	
	public void smartFire(double distance)
	{
		energy = getEnergy();
		
		if(energy > 40)
		{
			if(getGunHeat() <= 0)
			{
				fire(400/distance);
				out.println("Firing main canon " + 400/distance);
			}
		}
		else
		{
			
			if(getGunHeat() <=0);
			{
				fire((400/distance)/2);
				out.println("Firing Emergency canon " + (400/distance)/2);
			}
		}
	}


	/**
	 * onScannedRobot: What to do when you see another robot
	 */
	public void onScannedRobot(ScannedRobotEvent e) {
		// Scan many as possible like the tracker robot
		if (e.isSentryRobot())
		{
			return;
		}
		if (!e.isSentryRobot() && e.getEnergy() <= 45)
		{
			target = e.getName();
			out.println("New target: " + target);
		}
		else if (target != null && !e.getName().equals(target)) {
			out.println("Find target: " + e);
			return;
		}
		
		
		count = 0;
		
		
		distance = e.getDistance();
		smartFire(distance);
		
		
		//finish the low healt robot
		if(!e.isSentryRobot() && e.getEnergy() <= 45)
		{
			setTurnRight(e.getBearing());
			setTurnGunRight(e.getBearing() + (getHeading() - getRadarHeading()));
			if(distance >70)
			{
				setAhead(e.getDistance() - 100);
				//execute();
			}
			else if (distance < 75 || (getX() > size || getX() < 225 || getY() < 225 || getY() > size))
			{
				back(20);
			}
		}
		scan();
	}

	/**
	 * onHitByBullet: What to do when you're hit by a bullet
	 */
	public void onHitByBullet(HitByBulletEvent e) {
		// Replace the next line with any behavior you would like
	}
	
	/**
	 * onHitWall: What to do when you hit a wall
	 */
	public void onHitWall(HitWallEvent e) {
		// Replace the next line with any behavior you would like
		turnRight(90);
		scan();
		ahead(300);
	}	
}
