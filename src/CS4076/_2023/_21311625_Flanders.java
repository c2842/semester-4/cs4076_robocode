// Important to have

package CS4076._2023;

import robocode.*;

public class _21311625_Flanders extends AdvancedRobot {
  boolean movingForward;
  int gunDirection = 1;

  public void run() {

    while (true) {
      if (getOthers() < 10) {
        turnGunRight(360);
      } else {
        setAhead(40000);
        movingForward = true;
        setTurnRight(90);
        waitFor(new TurnCompleteCondition(this));
        setTurnLeft(180);
        waitFor(new TurnCompleteCondition(this));
        setTurnRight(180);
        waitFor(new TurnCompleteCondition(this));
      }
    }
  }

  /**
   * onHitWall:  Handle collision with wall.
   */
  public void onHitWall(HitWallEvent e) {
   
    reverseDirection();
  }

  /**
   * reverseDirection:  Switch from ahead to back &amp; vice versa
   */
  public void reverseDirection() {
    if (movingForward) {
      setBack(40000);
      movingForward = false;
    } else {
      setAhead(40000);
      movingForward = true;
    }
  }

  /**
   * onScannedRobot:  Fire!
   */
  public void onScannedRobot(ScannedRobotEvent e) {
    if (e.isSentryRobot()) {
      return;
    }
    if (getOthers() < 10) {
      setTurnRight(e.getBearing());
      
      setFire(3);
     
      setAhead(100);
      
      gunDirection = -gunDirection;
     
      setTurnGunRight(360 * gunDirection);
      
      execute();
    } else {
      fire(1);
    }
  }

  /**
   * onHitRobot:  Back up!
   */
  public void onHitRobot(HitRobotEvent e) {
    
    if (e.isMyFault()) {
      reverseDirection();
    }
  }
}
