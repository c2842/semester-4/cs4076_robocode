package CS4076._2023;

import robocode.*;
import java.awt.*;

public class _21326312_BrazilDiff extends AdvancedRobot {
	public void run() {
		setBodyColor(Color.blue);
		setGunColor(Color.yellow);
		setRadarColor(Color.white);
		setScanColor(Color.yellow);

		while (true) {
			setTurnRight(10000);
			setMaxVelocity(5);
			ahead(10000);
		}
	}

	public void onScannedRobot(ScannedRobotEvent e) {
		fire(3);
	}

	public void onHitRobot(HitRobotEvent e) {
		if (e.getBearing() > -10 && e.getBearing() < 10) {
			fire(3);
		}
		if (e.isMyFault()) {
			turnRight(10);
		}
	}
}
