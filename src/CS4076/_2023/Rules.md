# CS4076 2023 Robocode Rules
## Moodle Rules
Original page <<https://moodle2.csis.ul.ie/mod/wiki/view.php?id=12681>>.  
Moved some things around to fit this new format.

### File
The bot must be named ``_ID_Name.java`` (for example ``_12345678_Zoomer.java``).
Package should be: ``package CS4076._2023;``

### Arena

* It will be a Battle Royale.
* Scalable arena.
* The names will be hidden – To prevent teaming
* There will be 4 sentry turrets in the border to kill robots for leaving the battlefield. 
  There is a method to check if the robot scanned is a sentry turret, so try not to lock onto it.
* If there are more than 16 Players
  * The size will be 2000 x 2000, with a border with width of 200 in order to get the top 16.
  * 250 Rounds.
* Top 16 battle.
  * The size will be 1200 x 1200, with a border with width of 200, meaning the safe zone has a size of 800x800.
  * 1000 rounds.
* Stalls:
  * The robot with the lowest power will be killed by the admin. 
  * If there is two robots with the same power then it will be a coinflip to decide which one gets killed. 
  * This is repeated until there is no more stalling robots.

### Types of bot
Advanced Robots and Normal Robots are allowed to use.

### Score
We will use the default score system.

### Submission
* Merge request (see [Forking and merging instructions](../../../Instructions_Forking.md) for more info).
  * Preferred
* Emailed to the designated committee member (TBD)

## Reality
There were a few things above that could be improved for the future.

1. We didn't actually put a time on the rules for final submission.
   * Much of the final prepwork was done on our class discord.
   * Received a submission from an Erasmus student several hours after the competition.
   * Can be rectified for future year-groups.
   * We should have also put a link to the discord on Moodle.
2. We ran only one "pool" of competitors.
   * The 16+ one only ran for 100 rounds due to time constraints (took ~20 min, laptop was throttling).
   * The top 16 one for 1000 rounds was not run.
   * The figures of 250 and 1000 were pulled out of the air in teh initial committee meetings. 
     * We should have tested the time on the laptop that was going to run it.
   * This had the issue of discriminating against those whose bots would do better with fewer competitors.
3. ``Scalable arena``
   * We had originally planned to have the arena dynamically resize.
   * Would have required drastically modifying Robocode itself.
   * Deemed too time expensive to do so.
4. This rule document nor the forking/merging instructions were as organised prior to teh time of the competition.
5. We also had several people try to submit multi file bots, drastically over complicated.
   * Future competitions should explicitly state that it is a single file that can be submitted.
   * Many of the completion grade bots on Github/Gitlab are multifile ones
6. Underscores are used to prepend both the package and class as Java has a rule that the first character cannot be a number.