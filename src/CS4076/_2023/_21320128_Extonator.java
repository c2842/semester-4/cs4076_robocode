package CS4076._2023;

import robocode.AdvancedRobot;
import robocode.HitRobotEvent;
import robocode.ScannedRobotEvent;

import java.awt.*;

public class _21320128_Extonator extends AdvancedRobot {
  public void run() {
    setBodyColor(Color.PINK);
    setBulletColor(Color.YELLOW);
    setRadarColor(Color.PINK);
    while (true) {
      setTurnRight(10000);
      setMaxVelocity(5);
      ahead(10000);
    }
  }

  public void onScannedRobot(ScannedRobotEvent e) {
    if (!e.isSentryRobot()) {
      fire(3);
    }
  }

  public void onHitRobot(HitRobotEvent e) {
    if (e.getBearing() > -10 && e.getBearing() < 10) {
      fire(3);
    }
    if (e.isMyFault()) {
      turnRight(10);
    }
  }
}