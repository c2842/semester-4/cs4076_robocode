The bots behavior is to move around the safe zone targetting whatever bot is closest to it while not targeting the SentryBot.

How we did the movement?
The movement takes the size of the safe zone by using the getBorderSize() method provided by robocode. 
The bot then gets given a fixed value to stay away from the edge to make sure that it doesn't get hurt by the Sentry.
It then gets the height and width of the battlefield by the respective method calls [getBattleFieldHeight() and getBattleFieldWidth()].
It then gets the 4 corners by using the values that it recieved.
Then while the energy is greater than 0, we get the bot to spin the radar (will be explained later) before moving to the next corner in its run.
The actual movement is done using a goTo() method that takes in the destination x and y from the move() method. It works out the best angle to go based on the difference bewtween the destination co-ordinates and our bots current co-ordinates.
We then convert this into degrees while reducing the amount of turns that we need to make, for example, instead of going right three times to go left we turn left.

Scanning and targeting:
As specified above the targetting is done before the bot moves to the next corner.
The radar does a 306 degree scan. If it detects a bot it invokes the onScannedRobot() method.
This method then checks if it was the sentry that was scanned and if so ignores it. If it isn't he sentry bot and there isn't another target, it sets it up as the target. If it isn't a sentry bot and we already have a target, it compares the distance of this new target and the old target and discards the target that is further away. Then it breaks any scan that is ongoing.
For targeting with the gun, we work out the direction they are from us, point the gun in its general direction and then fire at it.
Before firing the bullet, we figure out how far away it is, if it is more than 300 units away we fire a bullet of strength 1.1 (any bullet of strength greater than one does bonus damage), if it is greater than 200 units away but closer then 300 units away, it fires a bullet of strength 2, or if it is closer, it fires the max allowed by the rules (in case they change).
If we are hit, it fires in the direction that we were hit from at max power.

When we die:
When our bot is destroyed we clear our target and set the target distance to the biggest value possible.
